# awscred

## Prerequisites
* aws-runas version 3.2.0 or higher must be installed in search path
  - https://neo.pearson.com/docs/DOC-786999
  - https://github.com/mmmorris1975/aws-runas
* .aws/config must have appropriate profiles with proper authentification
  - (for Pearson on 2022-04-25 saml_provider is azuread)
    - use sampleconfig for hints on setting up .aws/config
* Put awscred in search path (usually same place as aws-runas)
* Note:  This script has been tested on mac.  Should work on linux distros.  Will not work on windows.

## Usage
* Get list of profiles with:
  - awscred profiles
* Print sample ~/.aws/config valid as of 2022-04-25:
  - awscred sampleconfig
* Set env variable and cred file:
  - awscred <profile>
* Just set env variable (no credentials file):
  - awscred <profile> nofile

## Why awscred is useful:
* Open a new shell with AWS access environment variables set
  - Ususally used for aws cli or scripts started from command/terminal prompt
* Set temporary credentials for profile name in ~/.aws/credentials (unless use nofile)
  - different profiles can be used by different tools
* Note: .aws/credentials file is needed for ide/NoSQL workbench/...

